# dynamic-config-update



## Getting started

1. Есть сервис, написанный на Golang, работающий под нагрузкой на 10 серверах.
....В основном конфиги идентичны на всех серверах, но иногда требуется на каком-то из них поменять пару параметров.


Ответ:  
Основной проект, который я использую для Golang конфигурации: 
    https://github.com/spf13/viper

Проект позволяет отслеживать и перезагружать config files в случае изменения

```
viper.OnConfigChange(func(e fsnotify.Event) {
	fmt.Println("Config file changed:", e.Name)
})
viper.WatchConfig()
```

Но, что представляется более полезным, это ВСТРОЕННАЯ возможность использовать **_Remote Key/Value Store Support_** (etcd Consul Firestore NATS).


```
viper.AddSecureRemoteProvider("etcd","http://127.0.0.1:4001","/config/server0.json","/etc/secrets/mykeyring.gpg")
viper.SetConfigType("json") // because there is no file extension in a stream of bytes,  supported extensions are "json", "toml", "yaml", "yml", "properties", "props", "prop", "env", "dotenv"
err := viper.ReadRemoteConfig()

```

## Test

Изменяемые параметры меняются каждые 5 минут 
```
type conf struct {
	Var0 string `json:"Var0"`
	Var1 string `json:"Var1"`
}
```
С удаленного сервера
```
curl http://129.151.223.38:8090
```
Можно также установить у себя
```
git clone https://gitlab.com/different-tests/dynamic-config-update
cd dynamic-config-update
go run ./cmd/service
```
или
```
docker run -p 8090:8090 remotejob/dynamic-config-update:v0.1
```
curl http://localhost:8090  

Результат должен менятся каждые 5 минут.
