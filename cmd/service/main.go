package main

import (
	"encoding/json"
	"log"
	"net/http"
	"time"

	"github.com/spf13/viper"
	_ "github.com/spf13/viper/remote"
)

var (
	vp *viper.Viper
)

type conf struct {
	Var0 string `json:"Var0"`
	Var1 string `json:"Var1"`
}

func helloConfig(w http.ResponseWriter, r *http.Request) {

	var conf conf
	vp.Unmarshal(&conf)

	w.WriteHeader(http.StatusCreated)
	w.Header().Set("Content-Type", "application/json")
	resp := make(map[string]string)
	resp["message"] = "Status Created"
	jsonResp, err := json.Marshal(conf)
	if err != nil {
		log.Fatalf("Error happened in JSON marshal. Err: %s", err)
	}
	w.Write(jsonResp)
}

func init() {
	vp = viper.New()

	vp.SetConfigName("service") // Configuration fileName without the .TOML or .YAML extension
	vp.SetConfigType("json")

	err := vp.AddRemoteProvider("etcd3", "http://129.151.223.38:2379", "/config/server0.json")
	if err != nil { // Handle errors reading the config file
		log.Println(err)
	}
	err = vp.ReadRemoteConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		log.Println(err)
	}
}

func main() {

	go func() {
		for {
			time.Sleep(time.Second * 5) // delay after each request

			// currently, only tested with etcd support
			err := vp.WatchRemoteConfig()
			if err != nil {
				log.Println("unable to read remote config: %v", err)
				continue
			}
		}
	}()

	http.HandleFunc("/", helloConfig)

	http.ListenAndServe(":8090", nil)

}
